/*
 * Copyright (C) 2019 Alexander Simeonov.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.flogger.fluency.backend;

/**
 * Configures remote Fluency loggers via system proprties.
 *
 * <p>To configure set the following system properties (also see {@link FluencyBackendFactory}):
 *
 * <ul>
 *   <li>{@code flogger.remote_settings=com.agsimeonov.flogger.backend.fluentd.SystemPropertiesFluentdRemoteLoggerSettings#getInstance}.
 *   <li>{@code flogger.fluentd_host=<fluentd_host>}.
 *   <li>{@code flogger.fluentd_port=<fluentd_port>}.
 * </ul>
 */
public class FluencySystemPropertiesRemoteSettings implements FluencyRemoteSettings {

  static final String FLUENTD_HOST = "flogger.fluentd_host";
  static final String FLUENTD_PORT = "flogger.fluentd_port";

  private static final FluencyRemoteSettings INSTANCE = new FluencySystemPropertiesRemoteSettings();

  /** Configures remote Fluency loggers via system properties. */
  private FluencySystemPropertiesRemoteSettings() {}

  /**
   * Acquires a singleton FluencyRemoteLoggerSettings.
   *
   * @return the FluencyRemoteLoggerSettings singleton.
   */
  public static FluencyRemoteSettings getInstance() {
    return INSTANCE;
  }

  /**
   * Acquires the Fluentd host using a system property. fallback to localhost.
   *
   * <p>To configure set the following:
   *
   * <ul>
   *   <li>{@code flogger.fluentd_host=<fluentd_host>}.
   * </ul><br>
   *
   * @return the Fluentd host based on a system property, fallback to localhost.
   */
  @Override
  public String getHost() {
    String result = System.getProperty(FLUENTD_HOST);
    return result != null ? result : "localhost";
  }

  /**
   * Acquires the Fluentd port using a system property. fallback to 24224.
   *
   * <p>To configure set the following:
   *
   * <ul>
   *   <li>{@code flogger.fluentd_port=<fluentd_port>}.
   * </ul><br>
   *
   * @return the Fluentd port based on a system property, fallback to 24224.
   */
  @Override
  public int getPort() {
    try {
      return Integer.valueOf(System.getProperty(FLUENTD_PORT));
    } catch (Exception exception) {
      return 24224;
    }
  }
}
